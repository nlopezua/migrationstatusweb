﻿// Stolen from https://datatables.net/plug-ins/sorting/formatted-numbers
// because the default num-fmt gets thrown off by the " MB" suffix

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "formatted-MB-pre": function (a) {
        a = (a === null || a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
        return parseFloat(a);
    },

    "formatted-MB-asc": function (a, b) {
        return a - b;
    },

    "formatted-MB-desc": function (a, b) {
        return b - a;
    }
});