﻿
function lookupMigration(netId) {
    lookupMigrations([netId]);
}

function lookupMigrations(ids) {
    for (var i = 0; i < ids.length; i += 20) {
        var batch = ids.slice(i,i+20);
        $.getJSON("/api/MigrationStatus", { ids: batch })
            .done(function (resultList) {
                $.each(resultList, function (idx, data) {
                    var rowClass = '';
                    switch (data.status) {
                        case 'Completed':
                            rowClass = 'success';
                            break;
                        case 'InProgress':
                        case 'CompletionInProgress':
                        case 'Synced':
                            rowClass = 'active';
                            break;
                        case 'Queued':
                        case 'Suspended':
                            rowClass = 'info';
                            break;
                    }

                    statustable.row.add(data).nodes().to$().addClass(rowClass);
                });
                statustable.draw();
            });
    }
}
$(document).ready(function () {
    // Enable the DataTables jQuery plugin
    statustable = $('#statustable').DataTable({
        select: true,
        buttons: ['selectAll','copy','csv', {
            extend: 'selected',
            text: 'delete',
            action: function (e, dt, button, config) {
                var watchedSet = new Set(JSON.parse(localStorage.watchedNetIds));
                dt.rows({ selected: true }).every(function (idx, tableLoop, rowLoop) {
                    var who = this.data().identity;
                    console.log("Removing NetID: " + who)
                    watchedSet.delete(who);
                }).remove();
                localStorage.watchedNetIds = JSON.stringify(Array.from(watchedSet));
                dt.draw();
            }
        }],
        dom: 'Brftpil',
        columns: [
            { "data": "identity" },
            { "data": "duration" },
            { "data": "percentComplete" },
            { "data": "totalMailboxSize" },
            { "data": "itemCount" },
            { "data": "mailboxForward" },
            { "data": "lastUpdateTimestamp" },
            { "data": "batch" },
            { "data": "status" }
        ],
        columnDefs: [
            {
                "render": function (data, type, row) {
                    if (row.completionTimestamp) {
                        return data + " on " + row.completionTimestamp;
                    } else {
                        return data;
                    }
                },
                "targets": 8
            },
            {
                "targets": 5,
                "render": function (data, type, row) {
                    return data ? '&rarr;' : '';
                }
            },
            {
                "targets": 3,
                "type": "formatted-MB"
            }
        ]
    });

    // handler for selecting more NetIDs to list
    $('form#addnetidform').submit(function (event) {
        event.preventDefault();
        var netidbox = document.getElementById("netidbox");
        var input = netidbox.value;
        netidbox.value = "";

        // Split the input string on line breaks so you can paste in multiple NetIds
        var netIds = input.split(' ');
        var watchedSet = new Set(JSON.parse(localStorage.watchedNetIds));

        for (var i = 0; i < netIds.length; i++) {
            console.log("adding netid: " + netIds[i]);
            watchedSet.add(netIds[i]);
        }
        lookupMigrations(netIds);
        // Save the ammended list back to localStorage
        localStorage.watchedNetIds = JSON.stringify(Array.from(watchedSet));
    });
    // handler for importing a whole batch
    $('form#addbatchform').submit(function (event) {
        event.preventDefault();
        var batchbox = document.getElementById("batchbox");
        var input = batchbox.value;
        batchbox.value = "";

        $.getJSON('/api/batch/' + input).done(function (data) {
            // Split the input string on line breaks so you can paste in multiple NetIds
            var watchedSet = new Set(JSON.parse(localStorage.watchedNetIds));
 
            for (var i = 0; i < data.length; i++) {
                console.log("adding netid: " + data[i]);
                watchedSet.add(data[i]);
            }
            lookupMigrations(data);
            // Save the ammended list back to localStorage
            localStorage.watchedNetIds = JSON.stringify(Array.from(watchedSet));
        });
    });
    // lookup self 
    // The client/javascript side doesn't know who you are, but the 
    // authenticated API call does
    lookupMigration('self');

    // if any other NetIDs are saved in localStorage, look them up too
    if (typeof (Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        if (!localStorage.hasOwnProperty("watchedNetIds")) {
            localStorage.watchedNetIds = "[]";
        } else {
            // load all of the other watched NetIDs too
            var watchedArray = JSON.parse(localStorage.watchedNetIds);
            lookupMigrations(watchedArray);
        }
    }
});