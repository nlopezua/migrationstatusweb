﻿using System;
using StackExchange.Redis;
using System.Runtime.Serialization;

namespace MigrationStatusWebAPI.Models
{
    public class MigrationStatus
    {
        public string identity;
        public string status;
        public string percentComplete;
        public string badItemsEncountered;
        public string totalMailboxSize;
        public string duration;
        public string itemCount;
        public string lastUpdateTimestamp;
        public string completionTimestamp;
        public string batch;
        public bool mailboxForward = false;

        public MigrationStatus(string Identity, HashEntry[] values)
        {
            this.identity = Identity;
            foreach (HashEntry value in values)
            {
                switch (value.Name)
                {
                    case "status": this.status = value.Value; break;
                    case "batch": this.batch = value.Value; break;
                    case "percentComplete": this.percentComplete = value.Value; break;
                    case "badItemsEncountered": this.badItemsEncountered = value.Value; break;
                    case "totalMailboxSize": this.totalMailboxSize = value.Value; break;
                    case "duration": this.duration = value.Value; break;
                    case "itemCount": this.itemCount = value.Value; break;
                    case "lastUpdateTimestamp": this.lastUpdateTimestamp = value.Value; break;
                    case "completionTimestamp": this.completionTimestamp = value.Value; break;
                    case "mailboxForward": this.mailboxForward = true; break;
                }
            }
        }
    }
}