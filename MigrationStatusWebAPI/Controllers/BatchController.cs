﻿using StackExchange.Redis;
using System.Web.Http;

namespace MigrationStatusWebAPI.Controllers
{
    public class BatchController : ApiController
    {
        public string[] GetById(string id)
        {
            IDatabase db = RedisSharedConnection.Connection.GetDatabase();
            RedisValue[] statusHash = db.SetMembers("Batch:"+id);
            return statusHash.ToStringArray();
        }
    }
}