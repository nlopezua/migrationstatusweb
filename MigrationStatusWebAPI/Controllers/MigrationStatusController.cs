﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using StackExchange.Redis;

namespace MigrationStatusWebAPI.Controllers
{
    public class MigrationStatusController : ApiController
    {
        private string cleanId(string id)
        {
            if (id.Contains('\\')) // local AD authentication, strip the domain
            {
                id = id.Substring(id.LastIndexOf('\\') + 1);
            }
            if (id.Contains('@')) // Azure Authentication, strip the domain part
            {
                id = id.Substring(0, id.IndexOf('@'));
            }
            return id;
        }
        // cough up the data straight from Redis
        public Models.MigrationStatus GetById(string id)
        {
            if (id == null) { return null; };
            IDatabase db = RedisSharedConnection.Connection.GetDatabase();
            if (id == "self") {
                id = cleanId(User.Identity.Name);
            }
            HashEntry[] statusHash = db.HashGetAll(id);
            return new Models.MigrationStatus(id, statusHash);
        }

        public List<Models.MigrationStatus> GetByIds([FromUri] string[] ids)
        {
            List<Models.MigrationStatus> results = new List<Models.MigrationStatus>();
            IDatabase db = RedisSharedConnection.Connection.GetDatabase();
            foreach (string id in ids)
            {
                
                results.Add(this.GetById(id));
            }
            return results;
        }
    }
}
